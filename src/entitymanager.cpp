/* game
 * Copyright (C) 2014-2016 Scott Bishop <treewojima@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defines.hpp"
#include "entitymanager.hpp"

#include <algorithm>

#include "entity.hpp"
#include "events/dispatcher.hpp"

EntityManager::EntityManager() : _destroyed(true)
{
    //Events::Dispatcher::subscribe<Events::EntityCreated>(*this);
    Events::Dispatcher::subscribe<Events::ComponentCreated>(*this);
}

EntityManager::~EntityManager()
{
    if (!_destroyed) destroy();
}

void EntityManager::initialize()
{
    _destroyed = false;
    //_map.clear();
}

void EntityManager::destroy()
{
	if (_destroyed) return;

    // TODO: update this as necessary for additional cleanup procedures

    //for (auto &pair : _map)
    //{
    //}

    Events::Dispatcher::unsubscribe(*this);
    _map.clear();

    _destroyed = true;
}

std::shared_ptr<Entity> EntityManager::createEntity(const std::string &debugName)
{
    auto entity = std::shared_ptr<Entity>(new Entity(uuid::generate(), debugName));

    _map.emplace(entity->getUUID(), EntityComponentsPair(entity, ComponentMap()));

    Events::Dispatcher::raise<Events::EntityCreated>(entity);

    return entity;
}

void EntityManager::destroyEntity(UUID uuid)
{
    _map.erase(uuid);
}

std::shared_ptr<Entity> EntityManager::getEntity(UUID uuid)
{    
    try
    {
        return _map.at(uuid).first;
    }
    catch (std::out_of_range)
    {
        throw Exceptions::NoSuchEntity(uuid);
    }
}

/*void EntityManager::onEvent(const Events::EntityCreated &event)
{
    EntityComponentsPair pair;
    pair.first = event.entity;

    _map[event.entity->getUUID()] = pair;
}*/

void EntityManager::onEvent(const Events::ComponentCreated &event)
{
    auto component = event.component;
    auto uuid = component->getParent()->getUUID();

    // I don't think there'll ever be a case where the Entity isn't registered,
    // but just in case...
    try
    {
        _map.at(uuid).second.emplace(typeid(*component), component);
    }
    catch (std::out_of_range)
    {
        throw Exceptions::NoSuchEntity(uuid);
    }
}

std::string EntityManager::toString() const
{
	std::ostringstream ss;
	ss << "EntityManager[entityCount = " << _map.size() << "]";
	return ss.str();
}
