#include "defines.hpp"
#include "events/dispatcher.hpp"

Events::Dispatcher::EventSubscriberMap Events::Dispatcher::_map;
Events::Dispatcher::EventAsyncSubscriberMap Events::Dispatcher::_asyncMap;
