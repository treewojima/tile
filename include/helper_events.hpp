/* game
 * Copyright (C) 2014-2016 Scott Bishop <treewojima@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HELPER_EVENTS_HPP__
#define __HELPER_EVENTS_HPP__

#include "defines.hpp"
#include "events/base.hpp"
#include "vector.hpp"

namespace Events
{
    // SDL quit event, or any keypress tied to it
    struct Quit : public Base { using Base::Base; };

    // Mouse down
    struct MouseDown : public Base
    {
        enum class Button { Right, Left } button;
        Vector2i position;

        MouseDown(float dt, Button b, Vector2i pos)
            : Base(dt),
              button(b),
              position(pos) {}
    };

    // Key down
    struct KeyDown : public Base
    {
        static const uint8_t *keys;

        using Base::Base;
    };
}

#endif
