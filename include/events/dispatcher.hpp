/* game
 * Copyright (C) 2014-2016 Scott Bishop <treewojima@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EVENTS_DISPATCHER_HPP__
#define __EVENTS_DISPATCHER_HPP__

#include "defines.hpp"

#include <boost/core/demangle.hpp>
#include <functional>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <utility>

#include "events/base.hpp"
#include "events/subscriber.hpp"
#ifdef _DEBUG_NEW_EVENTS
#   include "logger.hpp"
#endif

namespace Events
{
    // TODO: This class is ripe for refactoring
    class Dispatcher final
    {
    private:
        Dispatcher() = default;

    public:
        template <class E, class T>
        static void subscribe(T &subscriber);

        template <class E, class T>
        static void unsubscribe(T &subscriber);
        template <class T>
        static void unsubscribe(T &subscriber);

        template <class E, class... Args>
        static void raise(Args&& ... args);

    private:
        template <class T>
        class FalseValue : std::false_type {};

        using SubscriberCallback = std::function<void(const Base &)>;
        using SubscriberPair = std::pair<Subscriber *, SubscriberCallback>;
        using SubscriberList = std::list<SubscriberPair>;
        using EventSubscriberMap = std::unordered_map<std::type_index, SubscriberList>;

        using AsyncSubscriberCallback = std::function<void(std::shared_ptr<Base>)>;
        using AsyncSubscriberPair = std::pair<AsyncSubscriber *, AsyncSubscriberCallback>;
        using AsyncSubscriberList = std::list<AsyncSubscriberPair>;
        using EventAsyncSubscriberMap = std::unordered_map<std::type_index, AsyncSubscriberList>;

        static EventSubscriberMap _map;
        static EventAsyncSubscriberMap _asyncMap;
    };
}

template <class E, class T>
void Events::Dispatcher::subscribe(T &subscriber)
{
    if constexpr (std::is_base_of_v<Subscriber, T>)
    {
        auto callback = [&subscriber](const Events::Base &event)
            { subscriber.onEvent(static_cast<const E &>(event)); };

        _map[typeid(E)].push_back(
                    std::make_pair(static_cast<Subscriber *>(&subscriber),
                                   callback));

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "subscriber " << subscriber
                  << " is listening for events of type "
                  << boost::core::demangle(typeid(E).name());
#endif
    }
    else if constexpr (std::is_base_of_v<AsyncSubscriber, T>)
    {
        auto callback = [&subscriber](std::shared_ptr<Events::Base> event)
            { subscriber.queueEvent(std::static_pointer_cast<E>(event)); };

        _asyncMap[typeid(E)].push_back(
                    std::make_pair(static_cast<AsyncSubscriber *>(&subscriber),
                                   callback));

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "asynchronous subscriber " << subscriber
                  << " is listening for events of type "
                  << boost::core::demangle(typeid(E).name());
#endif
    }
    else
    {
        static_assert(FalseValue<T>::value,
                      "Only classes of type (Async)Subscriber can subscribe to events");
    }
}

template <class E, class T>
void Events::Dispatcher::unsubscribe(T &subscriber)
{
    if constexpr (std::is_base_of_v<Subscriber, T>)
    {
        auto s = static_cast<Subscriber *>(&subscriber);
        auto i = _map.find(typeid(E));
        if (i != _map.end())
        {
            auto &list = i->second;
            for (auto j = list.begin(); j != list.end(); j++)
            {
                if (j->first == s)
                {
                    list.erase(j);
                    break;
                }
            }
        }

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "subscriber " << subscriber
                  << " is no longer listening for events of type "
                  << boost::core::demangle(typeid(E).name());
#endif
    }
    else if constexpr (std::is_base_of_v<AsyncSubscriber, T>)
    {
        auto s = static_cast<AsyncSubscriber *>(&subscriber);
        auto i = _asyncMap.find(typeid(E));
        if (i != _asyncMap.end())
        {
            auto &list = i->second;
            for (auto j = list.begin(); j != list.end(); j++)
            {
                if (j->first == s)
                {
                    list.erase(j);
                    break;
                }
            }
        }

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "asynchronous subscriber " << subscriber
                  << " is no longer listening for events of type "
                  << boost::core::demangle(typeid(E).name());
#endif
    }
    else
    {
        static_assert(FalseValue<T>::value,
                      "Only classes of type (Async)Subscriber can unsubscribe from events");
    }
}

template <class T>
void Events::Dispatcher::unsubscribe(T &subscriber)
{
    if constexpr (std::is_base_of_v<Subscriber, T>)
    {
        auto s = static_cast<Subscriber *>(&subscriber);

        for (auto &i : _map)
        {
            auto &list = i.second;
            for (auto j = list.begin(); j != list.end(); j++)
            {
                if (j->first == s)
                {
                    list.erase(j);
                    break;
                }
            }
        }

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "subscriber " << subscriber
                  << " is no longer listening for events of any type";
#endif
    }
    else if constexpr (std::is_base_of_v<AsyncSubscriber, T>)
    {
        auto s = static_cast<AsyncSubscriber *>(&subscriber);

        for (auto &i : _asyncMap)
        {
            auto &list = i.second;
            for (auto j = list.begin(); j != list.end(); j++)
            {
                if (j->first == s)
                {
                    list.erase(j);
                    break;
                }
            }
        }

#ifdef _DEBUG_NEW_EVENTS
        LOG_DEBUG << "asynchronous subscriber " << subscriber
                  << " is no longer listening for events of any type";
#endif
    }
    else
    {
        static_assert(FalseValue<T>::value,
                     "Only classes of type (Async)Subscriber can unsubscribe from events");
    }
}

template <class E, class... Args>
void Events::Dispatcher::raise(Args&& ... args)
{
    static_assert(std::is_base_of<Base, E>::value,
                  "Can only raise types derived from class Events::Base");

    auto event = std::make_shared<E>(std::forward<Args>(args)...);
    auto baseEvent = std::static_pointer_cast<Base>(event);

#ifdef _DEBUG_NEW_EVENTS
    LOG_DEBUG << "event raised: " << event;
#endif

    // First, queue it for asynchronous subscribers
    auto asyncList = _asyncMap[typeid(E)];
    for (auto &pair : asyncList)
    {
        pair.second(baseEvent);
    }

    // Next, fire off regular subscribers
    auto list = _map[typeid(E)];
    for (auto &pair : list)
    {
        pair.second(*baseEvent);
    }
}

#endif
