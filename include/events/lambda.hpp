/* game
* Copyright (C) 2014-2016 Scott Bishop <treewojima@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __EVENTS_LAMBDA_HPP__
#define __EVENTS_LAMBDA_HPP__

#include "defines.hpp"

#include <functional>

#include "dispatcher.hpp"

namespace Events
{
    template <class E>
    class LambdaSubscriber : public Subscriber
    {
    public:
        using LambdaType = std::function<void(E)>;

        LambdaSubscriber(LambdaType &lambda) : _lambda(lambda) {}

        LambdaType _lambda;
    };

    // Helper function that takes an anonymous onEvent() function and creates
    // and registers a Subscriber
    template <class E, class T, class Lambda>
    std::shared_ptr<T> createSubscriber(Lambda&& lambda)
    {
        auto subscriber = std::make_shared<LambdaSubscriber>(std::forward(lambda));
        Dispatcher::subscribe<E>(subscriber);
        return subscriber;
    }
}

#endif
